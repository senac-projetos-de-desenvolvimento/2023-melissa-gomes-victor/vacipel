

# Vacipel

Projeto do curso de Analise e Desenvolvimento de Sistemas — Faculdade de Tecnologia Senac Pelotas.

Vacipel tem como seu objetivo ser um sistema de gerenciamento de histórico de saúde voltada a animais.

Trazendo o controle do histórico completo da imunização dos animais de estimação. sendo completamente acessível em qualquer navegador e dispositivo.

Localização da documentação medium [documentação](
https://medium.com/@melissagomesvictor/pdii-projeto-vacipel-sistema-de-administra%C3%A7%C3%A3o-de-vacina%C3%A7%C3%A3o-animal-5c28a6029c82)

Importante: Alteração no .env do projeto na pasta root, acesso ao supabase adicionado para banca poder realizar testes.

## Table Of Content

- [Instalação](#Instalação)
    - [Venv](#Venv)
    - [Requirements.txt](#Requirements)
- [Subindo a aplicação](#Django)

## Instalação

Este repositorio contém toda a documentação do projeto vacipel.

Requerimentos:

- Python Runtime
- Python 3.11
- Venv instalado

### Venv

Utilizando o CMD do windos faça a instalação do pacote [VirtualVenv](https://virtualenv.pypa.io/en/latest/)

```Bash
pip install virtualenv
```
Realizada a instalação crie uma pasta para o projeto na area de trabalho.

```Bash
cd desktop
mkdir projeto
cd projeto
```

Crie um ambiente Venv:

```Bash
python -m venv
```
Ative o ambiente

```Bash
source venv/Scripts/activate
```
ou

```Bash
cd venv/Scripts

activate
```

### Requirements
Utilize o comando Git clone para clonar o projeto.

```Bash
Git clone https://gitlab.com/senac-projetos-de-desenvolvimento/2023-melissa-gomes-victor/vacipel.git
```

Instale tudo do requirements.txt já pronto do projeto. ou baixe eles individualmente.

```Bash
pip install -r requirements.txt
```


Realizado a instalação das dependencias, abra o projeto clonado e siga as instruções abaixo:

### Django

Com o venv ativo e na pasta do projeto vá até a pasta vacipel

```bash
cd vacipel
```
Rode o seguinte comando:
```python
    python manage.py runserver
```
Agora em um navegador abra uma aba e navege até http://localhost:8000.

As configurações do banco de dados serão enviadas separadamente a o professor responsavel.





### Itens do requirement.txt
```Text
asgiref==3.7.2
beautifulsoup4==4.12.3
branca==0.7.2
Brotli==1.1.0
certifi==2024.2.2
cffi==1.16.0
charset-normalizer==3.3.2
click==8.1.7
colorama==0.4.6
cryptography==42.0.5
defusedxml==0.7.1
dj-database-url==2.1.0
Django==4.2.11
django-allauth==0.61.1
django-bootstrap-v5==1.0.11
django-cockroachdb==4.2
django-pgcrypto-fields==2.6.0
folium==0.16.0
fontawesomefree==6.5.1
gunicorn==22.0.0
h11==0.14.0
idna==3.6
Jinja2==3.1.4
MarkupSafe==2.1.5
numpy==1.26.4
oauthlib==3.2.2
osm2geojson==0.2.5
packaging==24.0
pillow==10.2.0
psycopg2-binary==2.9.9
pycparser==2.21
PyJWT==2.8.0
python-dotenv==1.0.1
python3-openid==3.2.0
requests==2.31.0
requests-oauthlib==1.3.1
shapely==2.0.4
soupsieve==2.5
sqlparse==0.4.4
typing_extensions==4.10.0
tzdata==2024.1
urllib3==2.2.1
uvicorn==0.29.0
whitenoise==6.6.0
xyzservices==2024.4.0

```