from django import forms 
from .models import Animal, Vacina, Peso, Historico


    
class AnimalForm(forms.ModelForm):
    ESPECIE_CHOICES = [
        ('Cachorro', 'Cachorro'),
        ('Gato', 'Gato'),
        ('Aves', 'Aves'),
        ('Roedor', 'Roedor'),
        ('Reptel', 'Reptel'),
        ('Bovino', 'Bovino'),
        ('Suino', 'Suino'),
        ('Caprino', 'Caprino'),
        ('Ovino', 'Ovino'),
        ('Equino', 'Equino'),
    ]
    GENDER_CHOICE = [
        ('F','Femea'),
        ('M','Macho')
    ]
    especie = forms.ChoiceField(choices=ESPECIE_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))
    genero = forms.ChoiceField(widget=forms.RadioSelect(attrs={'class': 'form-check form-check-inline'}), choices=GENDER_CHOICE)
    # image = forms.ImageField(required=False,widget=forms.FileInput(attrs={'class': 'form-control'}))
    def clean(self):
        cleaned_data = super().clean()
        imagem = cleaned_data.get('imagem')

        if imagem and imagem != self.instance.imagem:
            pass

        return cleaned_data
    
    class Meta:
        model = Animal
        fields = (
            'nome', 'coloracao', 'raca', 'especie', 'genero', 'data_nasc', 'imagem', 'dono'
        )
        widgets = {
            'nome': forms.TextInput(attrs={'class':'form-control'}),
            'coloracao': forms.TextInput(attrs={'class':'form-control'}),
            'raca': forms.TextInput(attrs={'class':'form-control'}),
            'especie': forms.Select(attrs={'class':'form-control'}),
            'genero': forms.RadioSelect(attrs={'class':'form-control' }),
            'data_nasc': forms.DateInput( format=('%d/%m/%Y'),attrs={'class':'form-control','type': 'date'}),
            'imagem': forms.FileInput(attrs={'class':'form-control'}),
            'dono': forms.HiddenInput()
        }
        
class VacineForm(forms.ModelForm):
        TIPOS_CHOICES = [
            ('Mono','Monovalente'),
            ('Poly','Polyvalente')
        ]
        tipo = forms.ChoiceField(choices=TIPOS_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

        def clean(self):
            cleaned_data = super().clean()
            imagem = cleaned_data.get('imagem')

            if imagem and imagem != self.instance.imagem:
                pass

            return cleaned_data
        class Meta:
            model = Vacina
            fields = (
                'nome', 'atuacao', 'marca', 'tipo', 'fabricacao', 'expiracao', 'validade', 'aplicado', 'aplicador', 'aprovado', 'img_vac', 'animal_ref_vac'
            )
            widgets = {
                'nome': forms.TextInput(attrs={'class':'form-control'}),
                'atuacao': forms.TextInput(attrs={'class':'form-control'}),
                'marca': forms.TextInput(attrs={'class':'form-control'}),
                'fabricacao': forms.DateInput( format=('%d/%m/%Y'),attrs={'class':'form-control','type': 'date'}),
                'expiracao': forms.DateInput( format=('%d/%m/%Y'),attrs={'class':'form-control','type': 'date'}),
                'validade': forms.DateInput( format=('%d/%m/%Y'),attrs={'class':'form-control','type': 'date'}),
                'aplicado': forms.DateInput( format=('%d/%m/%Y'),attrs={'class':'form-control','type': 'date'}),
                'aplicador': forms.HiddenInput(attrs={'class':'form-control'}),
                'aprovado': forms.HiddenInput(attrs={'class':'form-control'}),
                # 'img_vac': forms.TextInput(attrs={'class':'form-control'}),
                'imagem': forms.FileInput(attrs={'class':'form-control'}),
                'animal_ref_vac': forms.HiddenInput()
                
            }
            
class PesoForm(forms.ModelForm):
    PESO_CHOICES = [
        ('G', 'g'),
        ('K', 'kg')
        ]
    peso_tipo = forms.ChoiceField(choices=PESO_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Peso
        fields = ('peso','data_pesagem','peso_tipo','animal_ref')
        widgets = { 
            'peso':forms.NumberInput(attrs={'class':'form-control'}),
            'data_pesagem':forms.DateInput( format=('%d/%m/%Y'),attrs={'class':'form-control','type': 'date'}),
            'peso_tipo':forms.TextInput(attrs={'class':'form-control'}),
            'animal_ref_id':forms.TextInput(attrs={'class':'form-control'}),
            }
        
        
class HistoricoForm(forms.ModelForm):
    HISTORICO_CHOICES = [
        ('Ml', 'mililitro'),
        ('Mg', 'micrograma'),
        ('Pipeta', 'pipeta por kg')
    ]

    quantidade_tipo = forms.ChoiceField(choices=HISTORICO_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Historico
        fields = ('med_nome','atuacao' ,'quantidade', 'quantidade_tipo','aplicado' ,'animal_ref_hist')

        widgets = { 
            'med_nome':forms.TextInput(attrs={'class':'form-control'}),
            'quantidade':forms.NumberInput(attrs={'class':'form-control'}),
            'atuacao':forms.TextInput(attrs={'class':'form-control'}),
            'quantidade_tipo':forms.Select(attrs={'class':'form-control'}),
            'aplicado':forms.DateInput( format=('%d/%m/%Y'),attrs={'class':'form-control','type': 'date'}),
            'animal_ref_hist_id':forms.HiddenInput()
            }