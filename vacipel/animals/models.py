from django.contrib.auth.models import User
from django.db import models
from pgcrypto import fields
from datetime import date

class Animal(models.Model):
    nome = fields.TextPGPSymmetricKeyField(max_length=100)
    coloracao = models.CharField(max_length=100)
    raca = models.CharField(max_length=100)
    especie = models.CharField(max_length=100)
    genero = models.CharField(max_length=10, choices=[('M', 'Macho'), ('F', 'Femea')], null=True, blank=True)
    data_nasc = models.DateField()
    imagem = models.ImageField(default='default.jpg', upload_to='Imagem/Animal/Foto')
    dono = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    
    def __str__(self):
        animal_fk = self.dono
        return f"{self.nome} - {animal_fk.username}"
    
    # def save(self, *args, **kwargs):
    #     self.imagem.upload_to = f'Imagem/Animal/Foto/{self.nome}'
    #     super().save(*args, **kwargs)

class Peso(models.Model):
    peso = models.FloatField()
    data_pesagem = models.DateField(default=date.today)  
    peso_tipo = models.CharField(max_length=5, choices=[('G', 'g'), ('K', 'Kg')], null=True, blank=True)
    animal_ref = models.ForeignKey(Animal, on_delete=models.CASCADE, null=True, blank=True)
    
    def __str__(self):
        peso_fk = self.animal_ref
        return f"{self.peso} {self.peso_tipo}, Data: {self.data_pesagem}"
# class 

class Vacina(models.Model):
    nome = fields.TextPGPSymmetricKeyField(max_length=100)
    atuacao = fields.TextPGPSymmetricKeyField(max_length=100)
    marca = fields.TextPGPSymmetricKeyField(max_length=100)
    tipo = models.CharField(max_length=20, choices=[('Mono', 'Monovalente'), ('Poly', 'Polyvalente')], null=True, blank=True)
    fabricacao = fields.DatePGPSymmetricKeyField()  
    expiracao = fields.DatePGPSymmetricKeyField()  
    validade = fields.DatePGPSymmetricKeyField(null=True, blank=True)
    aplicado = fields.DatePGPSymmetricKeyField(default=date.today)
    aplicador = fields.IntegerPGPSymmetricKeyField(null=True, blank=True)
    aprovado = models.BooleanField(default=0)
    img_vac = models.ImageField(default='default.jpg', upload_to='Imagem/Animal/Vacina/')
    animal_ref_vac = models.ForeignKey(Animal, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        vacina_fk = self.animal_ref_vac
        return f"{self.nome}-{self.marca}-{self.aplicado}-{vacina_fk.nome}"
    

class Historico(models.Model):
    med_nome = fields.TextPGPSymmetricKeyField(max_length=100)
    atuacao = fields.TextPGPSymmetricKeyField(max_length=100)
    quantidade = fields.IntegerPGPSymmetricKeyField()
    quantidade_tipo = models.CharField(max_length=20, choices=[('Ml', 'mililitro'), ('Mg', 'micrograma'), ('Pipeta', 'pipeta por kg')], null=True, blank=True)
    aplicado = fields.DatePGPSymmetricKeyField(default=date.today)
    animal_ref_hist = models.ForeignKey(Animal, on_delete=models.CASCADE, null=True, blank=True)


    def __str__(self):
        historico_fk = self.animal_ref_hist
        return f"{self.med_nome}-{self.quantidade}-{self.quantidade_tipo}-{historico_fk.nome}"
    