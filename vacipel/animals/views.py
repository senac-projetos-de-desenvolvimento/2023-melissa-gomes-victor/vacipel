from django.shortcuts import render, redirect,  get_object_or_404
from django.http import HttpResponse 
from django.template import loader
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from .models import Animal,Peso,Vacina,Historico
from perfil.models import Perfil, Endereco
from reportlab.lib.styles import *

from .forms import AnimalForm,VacineForm,HistoricoForm,PesoForm
from django.core.exceptions import *
from django.http import *
from django.http import FileResponse
import io
from reportlab.pdfgen import canvas
from reportlab.lib.units import *
from reportlab.lib import *
from reportlab.lib import colors
from reportlab.lib.pagesizes import *
from django.contrib.auth.decorators import login_required
from reportlab.platypus import *

########################################
##     ANIMAIS                         #
########################################

""" Aqui ficam os metodos dos animais, aonde é encontrado o crud completo """

@method_decorator(login_required, name='dispatch')
class AnimalDetailView(DetailView):
    """ classe e metodos privados para demonstrar os detalhes de cada animal """
    
    model = Animal
    template_name = 'bases/animal_detalhe.html'
    
    """ query na base dos objectos da classe Animal """
    
    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if obj:
            
            """ em todo o codigo vai haver uma restrição de vizualisação somente para o dono do animal """
            
            if self.request.user!= obj.dono:
                raise PermissionDenied("Permisão proibida, Este animal não pertence a você")
        return obj

    def get_context_data(self, **kwargs):
        
        """ criando o contexto do que deve aparecer na classe animais """
        
        context = super().get_context_data(**kwargs)
        
        """ tipo a lib pandas, se faz uma order pelo mais recente peso """
        
        Recent = Peso.objects.filter(animal_ref=self.object).order_by('-data_pesagem').first()
        
        """ pegando dos objetos modelos"""
        
        vacinas = Vacina.objects.filter(animal_ref_vac=self.object)
        historico = Historico.objects.filter(animal_ref_hist=self.object)

        """ adiciona ao contexto o mais recente """
        context['Recente'] = Recent
        context['Vacina'] = vacinas
        context['Historico'] = historico
        context['Peso'] = Peso.objects.filter(animal_ref=self.object)
        
        """ retorna este contexto ao metodo de view """
        
        return context



""" ver a lista do animal resrtingida ao dono """

@login_required(login_url="/accounts/login/")
def animal_list(request):
    if request.user.is_authenticated:
        animals = Animal.objects.filter(dono=request.user)
        context = {
            'animals': animals,
            # 'perfil': perfil,
            #'peso': peso
        }
    else:
        animals = Animal.objects.all().order_by('nome')
        context = {'animals': animals}
    return render(request, 'bases/animal_listados.html',context)


""" adicionar um animal ao nosso crud """
@login_required(login_url="/accounts/login/")   
def animal_put(request):
    if request.method == 'POST':
        form = AnimalForm(request.POST, request.FILES)
        if form.is_valid():
            
            dono_id = request.user  
            form.instance.dono = dono_id  
            form.save()  
            return redirect('animals')
        else:
            return HttpResponse(form.errors)
    else:
        form = AnimalForm()
    return render(request, 'bases/animal_formulario.html', {'form': form})

""" processador do form do animal """

def process_form(request):
    context = {'form': AnimalForm()}
    form = AnimalForm(request.POST, request.FILES)
    return render(request, 'bases/animal_formulario.html', context)
    #print(HttpResponse())
    
""" deleta o animal da base """
def delete_animal(request,id):
    animal = Animal.objects.get(pk=id)
    animal.delete()
    return redirect('/animals/')

""" da o update no animal """
def update_form(request,id):
    try:
        animal = Animal.objects.get(pk=id)
        if request.user != animal.dono:
            raise PermissionDenied("Permisão proibida, Este animal não pertence a você")

        form = AnimalForm(request.POST or None,request.FILES or None, instance=animal)
        if form.is_valid():
            form.save()
            return redirect('/animals/')
        template = loader.get_template('bases/animal_formulario_update.html')
        context = {
            'animal': animal,
            'form':form
        }
        return HttpResponse(template.render(context, request))
    except:
        raise Http404("Animal não encontrado .... ou sem permisão apara acessar")

    
####################################
#           VACINAS                #
####################################


""" adicionar uma vacina ao nosso animal """

@login_required(login_url="/accounts/login/")   
def process_vacine_form(request, id):
    #try:
        animal = Animal.objects.get(pk=id)
        if request.user != animal.dono:
            raise PermissionDenied("Permisão proibida, Este animal não pertence a você")
        form = VacineForm(request.POST or None,request.FILES or None) #instance=animal
        context = {'form': form, 'animal':animal}
        if form.is_valid():
            form.instance.animal_ref_vac_id = int(animal.id) 
            form.save()  
            return redirect('pet_detail',pk=id)

        #form = VacineForm(request.POST, request.FILES)
        return render(request, 'bases/vacina_formulario_animal.html', context)
        print(HttpResponse())
        
    #except:
        #raise Http404("Animal não encontrado .... ou sem permisão apara acessar")
    
    
@login_required(login_url="/accounts/login/")   
def vacina_put(request, id):
    if request.method == 'POST':
        form = VacineForm(request.POST,request.FILES)
        if form.is_valid():
            dono_id = request.user  
            form.instance.dono = dono_id  
            form.save()  
            redirect('pet_detail',pk=id)
        else:
            return HttpResponse(form.errors)
    else:
        form = AnimalForm()

    return render(request, 'bases/vacina_formulario_animal.html', {'form': form})


@login_required(login_url="/accounts/login/")   
def delete_vacina(request,id,id_animal):
    animal = Animal.objects.get(pk=id_animal)
    vacina = Vacina.objects.get(pk=id)
    if request.user != animal.dono:
            raise PermissionDenied("Permisão proibida, Este animal não pertence a você")
    vacina.delete()

    return redirect('pet_detail',pk=id_animal)



def update_vacinaform(request,id, id_animal):
    try:
        animal = Animal.objects.get(pk=id_animal)
        vacina = Vacina.objects.get(pk=id)
        # if request.user != animal.dono:
        #     raise PermissionDenied("Permisão proibida, Este animal não pertence a você")

        form = VacineForm(request.POST or None, instance=vacina)
        if form.is_valid():
            form.instance.animal_ref_vac_id = int(animal.id) 
            form.save()  
            return redirect('pet_detail',pk=id_animal)
        template = loader.get_template('bases/vacina_formulario_animal_update.html')
        context = {
            'vacina': vacina,
            'animal':animal,
            'form':form
        }
        return HttpResponse(template.render(context, request))
    except:
        raise Http404("Animal não encontrado .... ou sem permisão apara acessar")


    
####################################
#           HISTORICOS             #
####################################


@login_required(login_url="/accounts/login/")   
def process_historic_form(request, id):
    # try:
        animal = Animal.objects.get(pk=id)
        print(animal.id)
        if request.user != animal.dono:
            raise PermissionDenied("Permisão proibida, Este animal não pertence a você")
        form = HistoricoForm(request.POST or None) 
        context = {'form': form, 'animal':animal}
        if form.is_valid():
            form.instance.animal_ref_hist_id = int(animal.id)
            form.save()  
            return redirect('pet_detail',pk=id)

        return render(request, 'bases/historico_formulario_animal.html', context)
    # except:
    #     raise Http404("Animal não encontrado .... ou sem permisão apara acessar")
    
    

@login_required(login_url="/accounts/login/")   
def delete_historic(request,id,id_animal):
    animal = Animal.objects.get(pk=id_animal)
    historico = Historico.objects.get(pk=id)
    if request.user != animal.dono:
            raise PermissionDenied("Permisão proibida, Este animal não pertence a você")
    historico.delete()

    return redirect('pet_detail',pk=id_animal)



def update_historicform(request,id, id_animal):
    try:
        animal = Animal.objects.get(pk=id_animal)
        historic = Historico.objects.get(pk=id)
        
        if request.user != animal.dono:
            raise PermissionDenied("Permisão proibida, Este animal não pertence a você")

        form = HistoricoForm(request.POST or None, instance=historic)
        if form.is_valid():
            form.instance.animal_ref_hist_id = int(animal.id) 
            form.save()  
            return redirect('pet_detail',pk=id_animal)
        template = loader.get_template('bases/historico_formulario_animal_update.html')
        context = {
            'historico': historic,
            'animal':animal,
            'form':form
        }
        return HttpResponse(template.render(context, request))
    except:
        raise Http404("Animal não encontrado .... ou sem permisão apara acessar")


    
####################################
#           PESO                   #
####################################


@login_required(login_url="/accounts/login/")   
def process_peso_form(request, id):
    try:
        animal = Animal.objects.get(pk=id)
        print(animal.id)
        if request.user != animal.dono:
            raise PermissionDenied("Permisão proibida, Este animal não pertence a você")
        form = PesoForm(request.POST or None) 
        context = {'form': form, 'animal':animal}
        if form.is_valid():
            form.instance.animal_ref_id = int(animal.id)
            form.save()  
            return redirect('pet_detail',pk=id)

        return render(request, 'bases/peso_formulario_animal.html', context)
    except:
        raise Http404("Animal não encontrado .... ou sem permisão apara acessar")
    
    

def update_pesoform(request,id, id_animal):
    # try:
        animal = Animal.objects.get(pk=id_animal)
        peso = Peso.objects.get(pk=id)
        
        if request.user != animal.dono:
            raise PermissionDenied("Permisão proibida, Este animal não pertence a você")

        form = PesoForm(request.POST or None, instance=peso)
        if form.is_valid():
            form.instance.animal_ref_id = int(animal.id) 
            form.save()  
            return redirect('pet_detail',pk=id_animal)
        template = loader.get_template('bases/peso_formulario_animal_update.html')
        context = {
            'peso': peso,
            'animal':animal,
            'form':form
        }
        return HttpResponse(template.render(context, request))
    # except:
    #     raise Http404("Animal não encontrado .... ou sem permisão apara acessar")

def generate_pdf(request,id):
    animals = Animal.objects.filter(id=id)
    #print(animals)
    vacinas =  Vacina.objects.filter(animal_ref_vac_id=id)
    pesos =  Peso.objects.filter(animal_ref_id=id)
    historico =  Historico.objects.filter(animal_ref_hist_id=id)

    iobuffer = io.BytesIO()
    canva = canvas.Canvas(iobuffer, pagesize=letter,bottomup=0) 
    objs = canva.beginText()
    objs.setTextOrigin(inch,inch)
    objs.setFont("Helvetica", 14)
    styles = getSampleStyleSheet()

    # vacinas = Vacina.objects.all()
    objs_vac_list = [["Nome", "Atuação", "Marca","Tipo","Fabricação","Expiração","Validade","Aplicado"],]
    objs_hist_list = [["Nome", "Atuação", "Quantidade","Quantidade Tipo","Aplicado dia"],]
    objs_peso_list = [["Peso", "Peso_tipo", "Data Pesagem"],]
    objs_grid_0 = [["Nome","Genero","Raça"]]
    objs_grid_1 = [["Data Nascimento","","Especie"]]
    objs_grid_2 = [["",""]]

    objs_image = ''
    
    
    for animal in animals:
        g0 = []
        g0.append(animal.nome)
        if animal.genero == 'F':
            genero = 'Femea'
        elif animal.genero == 'M':
            genero = 'Macho'
        else: 
            genero = 'Outros'
        g0.append(genero)
        g0.append(animal.raca)
        objs_grid_0.append(g0)
        objs_image = animal.imagem
        
    for animal in animals:
        g1 = []
        g1.append(animal.data_nasc)
        g1.append('')
        g1.append(animal.especie)
        objs_grid_1.append(g1)

    
    for vacina in vacinas:
        lt = []
        lt.append(vacina.nome)
        lt.append(vacina.atuacao)
        lt.append(vacina.marca)
        lt.append(vacina.tipo)
        lt.append(str(vacina.expiracao)), 
        lt.append(str(vacina.validade)), 
        lt.append(str(vacina.aplicado)),
        # lt.append(vacina.img_vac),
        lt.append(str(vacina.fabricacao))
        objs_vac_list.append(lt)
    

    for hist in historico:
        lth = []
        lth.append(hist.med_nome)
        lth.append(hist.atuacao)
        lth.append(hist.quantidade)
        lth.append(hist.quantidade_tipo)
        lth.append(str(hist.aplicado))
        objs_hist_list.append(lth)
    
    
    for peso in pesos:
        ltp = []
        ltp.append(peso.peso)
        ltp.append(peso.peso_tipo)
        ltp.append(str(peso.data_pesagem))
        objs_peso_list.append(ltp)

    table_style = TableStyle([
        ('BACKGROUND', (0, 0), (-1, 0), colors.mediumseagreen),
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
        ('ALIGN', (0, 0), (-1, 0), 'CENTER'),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('FONTSIZE', (0, 0), (-1, 0), 14),
        ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
        ('BACKGROUND', (0, 1), (-1, -1), colors.beige),
        ('TEXTCOLOR', (0, 1), (-1, -1), colors.black),
        ('ALIGN', (0, 1), (-1, -1), 'CENTER'),
        ('FONTNAME', (0, 1), (-1, -1), 'Helvetica'),
        ('FONTSIZE', (0, 1), (-1, -1), 12),
        ('BOTTOMPADDING', (0, 1), (-1, -1), 6),
        ('GRID', (0, 0), (-1, -1), 1, colors.black),
    ])
    table_style_g1 = TableStyle([
        ('TEXTCOLOR', (0, 0), (-1, 0), colors.black),
        ('ALIGN', (0, 0), (-1, 0), 'CENTER'),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
        ('FONTSIZE', (0, 0), (-1, 0), 14),
        ('TEXTCOLOR', (0, 1), (-1, -1), colors.black),
        ('ALIGN', (0, 1), (-1, -1), 'CENTER'),
        ('FONTNAME', (0, 1), (-1, -1), 'Helvetica'),
        ('FONTSIZE', (0, 1), (-1, -1), 12),
        ('TOPPADDING', (0, 1), (-1, -1), 6),
        ('LEFTPADDING', (0, 1), (-1, -1), 6),
        ('RIGHTPADDING', (0, 1), (-1, -1), 6),
        ('BOTTOMPADDING', (0, 1), (-1, -1), 6),
    ])
    
    image_width = 2 * inch
    table_width = 6 * inch
    
    image_path = objs_image
    img = Image(image_path, width=image_width, height=2 * inch)   
    
    
    table = Table(objs_vac_list)
    table.setStyle(table_style )

    table2 = Table(objs_hist_list)
    table2.setStyle(table_style)

    table3 = Table(objs_peso_list)
    table3.setStyle(table_style)

    table4 = Table(objs_grid_0,colWidths=[table_width/len(objs_grid_0[0])]*len(objs_grid_0[0]))
    table4.setStyle(table_style_g1)
    
    table5 = Table(objs_grid_1,colWidths=[table_width/len(objs_grid_0[0])]*len(objs_grid_0[0]))
    table5.setStyle(table_style_g1)
    
    table6 = Table(objs_grid_2,colWidths=[table_width/len(objs_grid_0[0])]*len(objs_grid_0[0]))
    table5.setStyle(table_style_g1)
    
    doc = SimpleDocTemplate("report.pdf", pagesize=letter)
        

    
    doc.build([ img,Spacer(1, 1*inch),table4,table5 ,Spacer(1, 1*inch),Paragraph("<b>Vacinas</b>", styles["Heading1"]),table, Spacer(1, 1*inch),Paragraph("<b>Historico</b>", styles["Heading1"]),table2,Spacer(1, 1*inch),Paragraph("<b>Pesagem</b>", styles["Heading1"]),table3])

    return FileResponse(open("report.pdf", "rb"), as_attachment=True, filename="report.pdf")
