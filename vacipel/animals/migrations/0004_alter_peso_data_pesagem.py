# Generated by Django 4.2.11 on 2024-03-21 19:24

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('animals', '0003_peso_peso_tipo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='peso',
            name='data_pesagem',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
