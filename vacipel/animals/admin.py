from django.contrib import admin

# Register your models here.
from .models import Animal, Peso, Vacina, Historico

admin.site.register(Animal)

admin.site.register(Peso)

admin.site.register(Vacina)

admin.site.register(Historico)