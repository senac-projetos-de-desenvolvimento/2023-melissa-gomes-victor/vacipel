from django.urls import path
from . import views

urlpatterns = [
    path('animals/', views.animal_list, name='animals'),
    path('animals/form/', views.animal_put, name='animalsform'),
    path('animals/<int:pk>/', views.AnimalDetailView.as_view(), name='pet_detail'),
    path('animals/update/<int:id>/', views.update_form, name='updateanimalsform'),
    path('animals/delete/<int:id>/', views.delete_animal, name='deleteanimals'),
    
    # RELACIONADO AS VACINAS
    path('animals/<int:id>/vacine/form', views.process_vacine_form, name='vacineform'),
    path('vacine/delete/<int:id_animal>/<int:id>/', views.delete_vacina, name='vacinedelete'),
    path('vacine/update/<int:id_animal>/<int:id>/', views.update_vacinaform, name='vacineupdate'),
    
    # RELACIONADO AOS HISTORICOS
    path('animals/<int:id>/historic/form', views.process_historic_form, name='historicform'),
    path('historic/delete/<int:id_animal>/<int:id>/', views.delete_historic, name='historicdelete'),
    path('historic/update/<int:id_animal>/<int:id>/', views.update_historicform, name='historicupdate'),

    # RELACIONADO AO PESO
    path('animals/<int:id>/peso/form', views.process_peso_form, name='pesoform'),
    path('peso/update/<int:id_animal>/<int:id>/', views.update_pesoform, name='pesoupdate'),
    
    # RELACIONADO AO PDF
    path('animals/pdf/<int:id>', views.generate_pdf, name='pdfgen'),

]
