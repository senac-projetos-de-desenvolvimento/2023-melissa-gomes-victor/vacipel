from django.urls import path
from . import views

urlpatterns = [
    
    path('veterinario/animal/<int:pk>', views.UserAnimalDetailView.as_view(), name='user_detail'),
    path('veterinario/pesquisa/usuarios/', views.user_animal_list, name='user_search'),

    path('veterinario/vacine/<int:id>/form', views.process_vacine_form, name='vetvacineform'),
    path('veterinario/vacine/update/<int:id_animal>/<int:id>/', views.update_vacinaform, name='vetvacineupdate'),
    
    path('veterinario/historic/<int:id>/form', views.process_historic_form, name='vethistoricform'),
    path('veterinario/historic/update/<int:id_animal>/<int:id>/', views.update_historicform, name='vethistoricupdate'),

    path('veterinario/peso/<int:id>/form', views.process_peso_form, name='vetpesoform'),
    path('veterinario/peso/update/<int:id_animal>/<int:id>/', views.update_pesoform, name='vetpesoupdate'),
    
    
    path('veterinario/meus_clientes', views.client_list, name='list'),

]