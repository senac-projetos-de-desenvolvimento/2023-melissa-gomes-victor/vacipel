from django.shortcuts import render
from animals.forms import AnimalForm,VacineForm,HistoricoForm,PesoForm
from django.shortcuts import render, redirect,  get_object_or_404
from django.http import HttpResponse 
from django.template import loader
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from animals.models import Animal,Peso,Vacina,Historico
from perfil.models import Perfil, Endereco
from animals.forms import AnimalForm,VacineForm,HistoricoForm,PesoForm
from django.core.exceptions import *
from django.http import *
from django.contrib.auth.models import User

from django.contrib.auth.decorators import login_required


#############################
#         VACINAS           #
#############################
"""A IDEIA AQUI É REAPROVEITAR O CODIGO DOS OUTROS LOCAIS PARA REALIZAR OS CRUDS"""


@login_required(login_url="/accounts/login/")   
def process_vacine_form(request, id):
    #try:
        animal = Animal.objects.get(pk=id)
        ## Vendo se o perfil é veterinario
        perfil = Perfil.objects.get(user=request.user)

        
        """ REMOVEMOS A PERMISSION DENIED DE DONO E ADICIONAREMOS A DE VETERINARIO """
        if perfil.user_type != 'V':
            raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")

        form = VacineForm(request.POST or None) #instance=animal
        context = {'form': form, 'animal':animal}
        if form.is_valid():
            form.instance.animal_ref_vac_id = int(animal.id) 
            form.instance.aplicador = int(perfil.user_id) 
            form.instance.aprovado = True 

            form.save()  
            return redirect('user_detail',pk=id)

        #form = VacineForm(request.POST, request.FILES)
        return render(request, 'bases/vacina_formulario_veterinario.html', context)
        print(HttpResponse())
        


def update_vacinaform(request,id, id_animal):
    try:
        animal = Animal.objects.get(pk=id_animal)
        vacina = Vacina.objects.get(pk=id)
        perfil = Perfil.objects.get(user=request.user)

        if perfil.user_type != 'V':
            raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")

        form = VacineForm(request.POST or None, instance=vacina)
        if form.is_valid():
            form.instance.animal_ref_vac_id = int(animal.id) 
            form.instance.aplicador = int(perfil.user_id) 
            form.instance.aprovado = True 

            form.save()  
            return redirect('user_detail',pk=id_animal)
        template = loader.get_template('bases/vacina_formulario_veterinario_update.html')
        context = {
            'vacina': vacina,
            'animal':animal,
            'form':form
        }
        return HttpResponse(template.render(context, request))
    except:
        raise Http404("Animal não encontrado .... ou sem permisão apara acessar")




#############################
#         HISTORICO         #
#############################


@login_required(login_url="/accounts/login/")   
def process_historic_form(request, id):
    # try:
        animal = Animal.objects.get(pk=id)
        perfil = Perfil.objects.get(user=request.user)

        if perfil.user_type != 'V':
            raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")
        
        form = HistoricoForm(request.POST or None) 
        context = {'form': form, 'animal':animal}
        if form.is_valid():
            form.instance.animal_ref_hist_id = int(animal.id)
            form.save()  
            return redirect('pet_detail',pk=id)

        return render(request, 'bases/historico_formulario_veterinario.html', context)
    
    

def update_historicform(request,id, id_animal):
    try:
        animal = Animal.objects.get(pk=id_animal)
        historic = Historico.objects.get(pk=id)
        
        perfil = Perfil.objects.get(user=request.user)

        if perfil.user_type != 'V':
            raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")
        
        form = HistoricoForm(request.POST or None, instance=historic)
        if form.is_valid():
            form.instance.animal_ref_hist_id = int(animal.id) 
            form.save()  
            return redirect('pet_detail',pk=id_animal)
        template = loader.get_template('bases/historico_formulario_veterinario_update.html')
        context = {
            'historico': historic,
            'animal':animal,
            'form':form
        }
        return HttpResponse(template.render(context, request))
    except:
        raise Http404("Animal não encontrado .... ou sem permisão apara acessar")



#############################
#         PESOS             #
#############################


@login_required(login_url="/accounts/login/")   
def process_peso_form(request, id):
    try:
        animal = Animal.objects.get(pk=id)
        perfil = Perfil.objects.get(user=request.user)

        if perfil.user_type != 'V':
            raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")
        
        form = PesoForm(request.POST or None) 
        context = {'form': form, 'animal':animal}
        if form.is_valid():
            form.instance.animal_ref_id = int(animal.id)
            form.save()  
            return redirect('pet_detail',pk=id)

        return render(request, 'bases/peso_formulario_veterinario.html', context)
    except:
        raise Http404("Animal não encontrado .... ou sem permisão apara acessar")
    
    

def update_pesoform(request,id, id_animal):
    # try:
        animal = Animal.objects.get(pk=id_animal)
        peso = Peso.objects.get(pk=id)
        
        perfil = Perfil.objects.get(user=request.user)

        if perfil.user_type != 'V':
            raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")
        
        form = PesoForm(request.POST or None, instance=peso)
        if form.is_valid():
            form.instance.animal_ref_id = int(animal.id) 
            form.save()  
            return redirect('pet_detail',pk=id_animal)
        template = loader.get_template('bases/peso_formulario_veterinario_update.html')
        context = {
            'peso': peso,
            'animal':animal,
            'form':form
        }
        return HttpResponse(template.render(context, request))
    # except:
    #     raise Http404("Animal não encontrado .... ou sem permisão apara acessar")


###########################
#      VET CLI VIEWS      #
###########################

""" PARA REALIZAR AS QUERIES """
@login_required(login_url="/accounts/login/")   
def user_animal_list(request):
    qs = Animal.objects.all()
    perfil = Perfil.objects.get(user=request.user)

    if perfil.user_type != 'V':
        raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")
        
    nome_animal_query = request.GET.get('nome_animal')
    nome_dono_query = request.GET.get('nome_dono')
    email_dono_query = request.GET.get('email_dono')
    primeiro_nome_query = request.GET.get('primeiro_nome')
    segundo_nome_query = request.GET.get('segundo_nome')
    animal_coloracao_query = request.GET.get('animal_coloracao')
    animal_especie_query = request.GET.get('animal_especie')

    
    if nome_animal_query != '' and nome_animal_query is not None:
        qs = qs.filter(nome__icontains=nome_animal_query)
        
        
    elif nome_dono_query != '' and nome_dono_query is not None:
        """ QUERIES DE DONO """
        try:
            user = User.objects.get(username=nome_dono_query)
            qs = Animal.objects.filter(dono=user)
        except User.DoesNotExist:
            qs = []
    
    elif email_dono_query != '' and email_dono_query is not None:
        try:
            user = User.objects.get(email=email_dono_query)
            qs = Animal.objects.filter(dono=user)
        except User.DoesNotExist:
            qs = []
    elif primeiro_nome_query != '' and primeiro_nome_query is not None:
        try:
            user = User.objects.get(first_name=primeiro_nome_query)
            qs = Animal.objects.filter(dono=user)
        except User.DoesNotExist:
            qs = []
    elif segundo_nome_query != '' and segundo_nome_query is not None:
        try:
            user = User.objects.get(last_name=segundo_nome_query)
            qs = Animal.objects.filter(dono=user)
        except User.DoesNotExist:
            qs = []
    elif animal_coloracao_query != '' and animal_coloracao_query is not None:
        qs = qs.filter(coloracao__icontains=animal_coloracao_query)
        
    elif animal_especie_query != '' and animal_especie_query is not None:
        qs = qs.filter(especie__icontains=animal_especie_query)

    """RENDERIZA O CONTEXTO SEMPRE"""
    context = {
            'animals': qs,
        }
    
    return render(request, 'bases/pesquisa_usuarios_animais_veterinario.html',context)



@method_decorator(login_required, name='dispatch')
class UserAnimalDetailView(DetailView):
    """ classe e metodos privados para demonstrar os detalhes de cada animal """
    
    model = Animal
    template_name = 'bases/veterinario_animal_detalhe.html'
    """ query na base dos objectos da classe Animal """
    
    
    
    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        #print(obj)
        #print(self.request.user)
        perfil = Perfil.objects.get(user=self.request.user)
        if perfil.user_type != 'V':
            raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")
        
        return obj

    def get_context_data(self, **kwargs):
        
        """ criando o contexto do que deve aparecer na classe animais """
        
        context = super().get_context_data(**kwargs)
        Recent = Peso.objects.filter(animal_ref=self.object).order_by('-data_pesagem').first()
        vacinas = Vacina.objects.filter(animal_ref_vac=self.object)
        historico = Historico.objects.filter(animal_ref_hist=self.object)
        """ PEGANDO O DONO DO MY CONTEXT """
        dono = str(self.object)
        dono = dono.split("-")[1].replace(" ", "")
        usuario = User.objects.get(username=dono)


        """ adiciona ao contexto o mais recente """
        context['Recente'] = Recent
        context['Vacina'] = vacinas
        context['Historico'] = historico
        context['Usuario'] = usuario

        context['Peso'] = Peso.objects.filter(animal_ref=self.object)
        
        """ retorna este contexto ao metodo de view """
        
        return context




###########################
#  CLIENTES VIEWS         #
###########################

@login_required(login_url="/accounts/login/")   
def client_list(request):
    model = Vacina
    #vacina = Perfil.objects.all()

    perfil = Perfil.objects.get(user=request.user)
    if perfil.user_type != 'V':
        raise PermissionDenied("Permisão proibida, Não autorizado a este recurso")
    
    vacina = Vacina.objects.filter(aplicador=perfil.user_id)
    animal = Animal.objects.filter(vacina__in=vacina).distinct()

    """RENDERIZA O CONTEXTO SEMPRE"""
    context = {
            'query': animal,
        }
    
    return render(request, 'bases/veterinario_clientes.html',context)



