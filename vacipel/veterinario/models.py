from django.contrib.auth.models import User
from django.db import models
from pgcrypto import fields
from animals.models import Animal
from perfil.models import Perfil
from datetime import date

class Veterinario(models.Model):
    crmv = fields.TextPGPSymmetricKeyField()
    cpf = fields.TextPGPSymmetricKeyField(max_length=12,null=True)
    cnpj = fields.TextPGPSymmetricKeyField(max_length=35,null=True)
    is_valid = models.BooleanField(default=0)
    perfil_ref = models.ForeignKey(Perfil, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f"{self.crmv}-{self.cpf}"
    