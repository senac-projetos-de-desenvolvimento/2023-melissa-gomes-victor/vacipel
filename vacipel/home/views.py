from django.shortcuts import render
from perfil.models import Perfil

def home(request):
    if request.user.is_authenticated:
            perfil = Perfil.objects.get(user=request.user)
            print(perfil.user_type)
            id = int(perfil.id)
            perfil_id = f'/accounts/profile/{id}'
        
    else:
        perfil_id = '/accounts/login/'
        
    items_before = [
        {"title": "Mapa e Estatisticas","icon":"fas fa-map",  "button_text": "Acesse o Mapa", "link":"geo-map", "style":"background: linear-gradient(to right, #0a504ac3, #38ef7dc3); border-radius: 10px;"},
        {"title": "Sobre Nos","icon":"fas fa-file-contract",  "button_text": "Acesse Sobre", "link":"https://gitlab.com/senac-projetos-de-desenvolvimento/2023-melissa-gomes-victor/vacipel",  "style":"background: linear-gradient(to right, #373b44c3, #4286f4c3);border-radius: 10px;"},
        {"title": "Cadastro","icon":"fas fa-user",  "button_text": "Cadastre-se", "link":"#",  "style":"background: linear-gradient(to right, #0a504ac3, #38ef7dc3);border-radius: 10px;"},
        {"title": "Documentação","icon":"fas fa-file",  "button_text": "Documentação", "link":"https://medium.com/@melissagomesvictor/pdii-projeto-vacipel-sistema-de-administra%C3%A7%C3%A3o-de-vacina%C3%A7%C3%A3o-animal-5c28a6029c82",  "style":"background: linear-gradient(to right, #373b44c3, #4286f4c3);border-radius: 10px;"},
    ]
    if request.user.is_authenticated:
        if perfil.user_type == 'V':
            items_after = [  {"title": "Pets","icon":"fas fa-paw",  "button_text": "Acesse Seus Animais", "link":"animals/", "style":"background: linear-gradient(to right, #0a504ac3, #38ef7dc3);border-radius: 10px;"},
            {"title": "Meu Perfil","icon":"fas fa-user-edit",  "button_text": "Acesse Seu Perfil", "link":"accounts/profile/", "style":"background: linear-gradient(to right, #373b44c3, #4286f4c3);border-radius: 10px;"},
            {"title": "Meus Clientes","icon":"fas fa-handshake",  "button_text": "Acesse os seus clientes", "link":"veterinario/meus_clientes/", "style":"background: linear-gradient(to right, #373b44c3, #4286f4c3);border-radius: 10px;"},
            {"title": "Procurar Pet","icon":"fas fa-search",  "button_text": "Pesquise Pets e Usuarios", "link":"veterinario/pesquisa/usuarios/", "style":"background: linear-gradient(to right, #0a504ac3, #38ef7dc3);border-radius: 10px;"},
            ]
            
        else:
            items_after = [  {"title": "Pets","icon":"fas fa-paw",  "button_text": "Acesse Seus Animais", "link":"animals/", "style":"background: linear-gradient(to right, #0a504ac3, #38ef7dc3);border-radius: 10px;"},
            {"title": "Meu Perfil","icon":"fas fa-user-edit",  "button_text": "Acesse Seu Perfil", "link":"accounts/profile/", "style":"background: linear-gradient(to right, #373b44c3, #4286f4c3);border-radius: 10px;"},
            ]

    
    login_icon = [{"i":"fas fa-user","ref":"/accounts/profile"},{"i":"fas fa-sign-out-alt","ref":"/accounts/logout"}]
    if request.user.is_authenticated:
        items = items_before[:-2] + [item for item in items_after if request.user.is_authenticated]
        
    else:
        items = items_before
    return render (request, 'bases/modelo.html', {'items': items,'icons':login_icon, 'ID':perfil_id})
