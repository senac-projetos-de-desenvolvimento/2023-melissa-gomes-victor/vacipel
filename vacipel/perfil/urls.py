from django.urls import path
from . import views

urlpatterns = [
    #path('accounts/profile/', views.perfil, name='perfil'),
    path('accounts/profile/<int:pk>', views.ProfileDetailView.as_view(), name='perfil'),
    path('endereco/update/<int:id_perfil>/<int:id>/', views.update_enderecoform, name='enderecoupdate'),
    # path('accounts/signup/vet/', views.signup_view, name='signupvet')

]
