from django.shortcuts import render, redirect,  get_object_or_404
from django.http import HttpResponse 
from django.template import loader
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from .models import Perfil,Endereco
from django.contrib.auth.models import User
from .forms import EnderecoForm
from .forms import SignupForm

from django.core.exceptions import *
from django.http import *
from django.contrib.auth.decorators import login_required


@method_decorator(login_required, name='dispatch')
class ProfileDetailView(DetailView):
    model = Perfil
    template_name = 'bases/profile.html'
    
    def get_object(self, queryset=None):
        obj = super().get_object(queryset)
        if obj:
            
            """ em todo o codigo vai haver uma restrição de vizualisação somente para o dono do perfil """
            if self.request.user.id != obj.user_id:
                print(obj.user_id)
                raise PermissionDenied("Permisão proibida, Este perfil não pertence a você")
            return obj


    def get_context_data(self, **kwargs):
        """ criando o contexto do que deve aparecer na classe animais """
        
        context = super().get_context_data(**kwargs)
        print(context)
        """ pegando dos objetos modelos"""
        
        endereco = Endereco.objects.filter(perfil_usuario_id=self.object)

        context['Perfil'] = Perfil
        context['Endereco'] = endereco
        
        """ retorna este contexto ao metodo de view """
        
        return context


@login_required(login_url="/accounts/login/")   
def update_enderecoform(request,id, id_perfil):
    
        perfil = Perfil.objects.get(pk=id_perfil)
        endereco = Endereco.objects.get(pk=id)


        if request.user.id != perfil.user_id or endereco.perfil_usuario != perfil:
            raise PermissionDenied("Permisão proibida, Este perfil não pertence a você")

        form = EnderecoForm(request.POST or None, instance=endereco)
        
        if form.is_valid():
            form.instance.perfil_usuario = perfil
            form.save()  

                    
                    
        template = loader.get_template('bases/endereco_formulario_update.html')
        context = {
            'endereco': endereco,
            'perfil':perfil,
            'form':form
        }
        return HttpResponse(template.render(context, request))
    
    
    