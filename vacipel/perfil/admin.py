from django.contrib import admin
from .models import Perfil, Endereco

admin.site.register(Perfil)
admin.site.register(Endereco)
