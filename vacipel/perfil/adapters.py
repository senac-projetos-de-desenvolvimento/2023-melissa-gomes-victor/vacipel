from allauth.account.adapter import DefaultAccountAdapter
from .models import Perfil  # Adjust the import path according to your project structure

# class LogInNextAdapter(DefaultAccountAdapter):
#     def get_login_redirect_url(self, request):
#         if request.user.is_authenticated:
#             user_id = str(request.user.id)
#             print(user_id)
#         next_path = request.GET.get('next', '/accounts/profile/')
#         if next_path:
#             return next_path
#         return super().get_login_redirect_url(request)

class LogInNextAdapter(DefaultAccountAdapter):
    def get_login_redirect_url(self, request):
        if request.user.is_authenticated:
            perfil = Perfil.objects.get(user=request.user)
            perfil_id = int(perfil.id)
            print(f'perfil id: {perfil_id}' )
            next_path = request.GET.get('next', f'/accounts/profile/{perfil_id}')
        if next_path:
            return next_path
        
    def get_signup_redirect_url(self, request, form, **kwargs):
        perfil = Perfil.objects.get(user=request.user)
        user_type = perfil.user_type
        print(user_type)
        # if user_type == 'C':
        #     return '/accounts/confirm-email/'
        # elif user_type == 'V':
        #     return '/'
        # else:
        #     return super().get_signup_redirect_url(request, form, **kwargs)
        return super().get_login_redirect_url(request)