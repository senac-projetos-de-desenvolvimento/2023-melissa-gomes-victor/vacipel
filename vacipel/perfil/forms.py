from django import forms
import requests 
from .models import Endereco, Perfil
from maps.models import GeoMap
from veterinario.models import Veterinario
from django.contrib.auth.forms import UserCreationForm
from allauth.account.forms import SignupForm, LoginForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy

""" MODELO DE GERACAO DE UM CADASTRO MAIS ROBUSTO """

class SignupForm(SignupForm):
    image = forms.ImageField(required=False,widget=forms.FileInput(attrs={'class': 'form-control'}))
    user_type = forms.CharField(widget=forms.Select(choices=[('C', 'Dono de Pet'), ('V', 'Veterinario')], attrs={'class': 'form-select'}))
    numero = forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    rua= forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    bairo = forms.CharField(widget=forms.Select(choices=[('Centro', 'Centro'),('Fragata', 'Fragata'),('Barragem', 'Barragem'),('Três Vendas', 'Três Vendas'),('Areal', 'Areal'),('Laranjal', 'Laranjal'),('São Gonçalo|Porto', 'São Gonçalo|Porto')], attrs={'class': 'form-select'}))
    complemento = forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    cep = forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    username = forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    # password1 = forms.CharField(max_length=255, required=True, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    # password2 = forms.CharField(max_length=255, required=True, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    email = forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
            model = User
            fields = ('')
    def __init__(self, *args, **kwargs):
            super(SignupForm,self).__init__(*args, **kwargs)
            self.fields['password1'].widget.attrs['class'] = 'form-control'
            self.fields['password2'].widget.attrs['class'] = 'form-control'

    def save(self, *args, **kwargs):
        user = super(SignupForm, self).save(*args, **kwargs)


        perfil = Perfil.objects.create(
            user_id=user.id,
            image=self.cleaned_data['image'],
            user_type=self.cleaned_data['user_type']
        )

        endereco = Endereco.objects.create(
            perfil_usuario_id=perfil.id,
            rua=self.cleaned_data['rua'],
            numero=self.cleaned_data['numero'],
            complemento=self.cleaned_data['complemento'],
            bairo=self.cleaned_data['bairo'],
            cep=self.cleaned_data['cep'],
            #perfil_usuario_id=self.cleaned_data['perfil_usuario_id']
        )
        
        
        def get_geo(address):
            url = "https://nominatim.openstreetmap.org/search"
            params = {
                "q": address,
                "format": "json",
                "limit": 1
            }
            headers = {
                "User-Agent": "Vacipel_TCC/1.0", 
                "Referer": "https://gitlab.com/senac-projetos-de-desenvolvimento/2023-melissa-gomes-victor/vacipel"  
            }
            response = requests.get(url, params=params, headers=headers)
            data = response.json()

            if data and len(data) > 0:
                place_id = data[0]["place_id"]
                osm_id = data[0]["osm_id"]
                lat = data[0]["lat"]
                lon = data[0]["lon"]
                name = data[0]["name"]
                return place_id,osm_id, lat, lon, name
            else:
                print("Sem Resultados")
                return 0, 0, '0', '0', '0'
        
        
        end = f'{endereco.rua} {endereco.numero} {endereco.bairo} Pelotas RS'
        values = get_geo(end)
        #print(f'endereço: {end}')
        geo_map = GeoMap.objects.create(
            place_id = values[0],
            osm_id = values[1],
            lat = values[2],
            lon = values[3],
            name = values[4],
            bairo = endereco.bairo,
            user_pk = user
        )

        return user

""" MODELO PARA GERACAO DE UM LOGIN MAIS BONITO """

class LoginForm(LoginForm):
    login = forms.CharField( max_length=255,required=True,widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=255, required=True, widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    class Meta:
        model = User
        fields = ('')
        
    
    def __init__(self, *args, **kwargs):
        super(LoginForm,self).__init__(*args, **kwargs)
        self.fields['login'].widget.attrs['class'] = 'form-control'
    pass




""" MODELO PARA ALTERACAO DO ENDEREÇO """

class EnderecoForm(forms.ModelForm):

    BAIRO_CHOICES = [
        ('Centro', 'Centro'),
        ('Fragata', 'Fragata'),
        ('Barragem', 'Barragem'),
        ('Três Vendas', 'Três Vendas'),
        ('Areal', 'Areal'),
        ('Laranjal', 'Laranjal'),
        ('São Gonçalo|Porto', 'São Gonçalo|Porto')
        
    ]

    bairo = forms.ChoiceField(choices=BAIRO_CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

    class Meta:
        model = Endereco
        fields = ('rua','cep','numero','bairo','complemento','perfil_usuario')

        widgets = { 
            'rua':forms.TextInput(attrs={'class':'form-control'}),
            'cep':forms.TextInput(attrs={'class':'form-control'}),
            'numero':forms.TextInput(attrs={'class':'form-control'}),
            'bairo':forms.Select(attrs={'class':'form-control'}),
            'complemento':forms.TextInput(attrs={'class':'form-control'}),
            'perfil_usuario':forms.HiddenInput()
            }

