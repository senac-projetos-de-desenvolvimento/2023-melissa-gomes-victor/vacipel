from django.contrib.auth.models import User
from django.db import models
from pgcrypto import fields

class Perfil(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_type = models.CharField(max_length=1, choices=[('C', 'Type C'), ('V', 'Type V')])
    image = models.ImageField(default='default.jpg', upload_to='Imagem/User/Profile')
    
    def __str__(self):
        return self.user.username
    
    def save(self, *args, **kwargs):
        self.image.upload_to = f'Imagem/User/Profile/{self.user.username}'
        super().save(*args, **kwargs)
    
class Endereco(models.Model):
    perfil_usuario = models.ForeignKey(Perfil, on_delete=models.CASCADE)
    rua = fields.TextPGPSymmetricKeyField(max_length=255)
    numero = fields.TextPGPSymmetricKeyField(max_length=100)
    complemento = fields.TextPGPSymmetricKeyField(max_length=150)
    bairo = fields.TextPGPSymmetricKeyField(max_length=255)
    cep = fields.TextPGPSymmetricKeyField(max_length=10)

    def __str__(self):
        return f"{self.rua}, {self.bairo}, {self.cep}"
