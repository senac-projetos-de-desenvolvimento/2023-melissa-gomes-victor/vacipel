# Generated by Django 4.2.11 on 2024-03-18 16:47

from django.db import migrations
import pgcrypto.fields


class Migration(migrations.Migration):

    dependencies = [
        ('perfil', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='endereco',
            name='bairo',
            field=pgcrypto.fields.TextPGPSymmetricKeyField(max_length=255),
        ),
        migrations.AlterField(
            model_name='endereco',
            name='cep',
            field=pgcrypto.fields.TextPGPSymmetricKeyField(max_length=10),
        ),
        migrations.AlterField(
            model_name='endereco',
            name='complemento',
            field=pgcrypto.fields.TextPGPSymmetricKeyField(max_length=150),
        ),
        migrations.AlterField(
            model_name='endereco',
            name='numero',
            field=pgcrypto.fields.TextPGPSymmetricKeyField(max_length=100),
        ),
        migrations.AlterField(
            model_name='endereco',
            name='rua',
            field=pgcrypto.fields.TextPGPSymmetricKeyField(max_length=255),
        ),
    ]
