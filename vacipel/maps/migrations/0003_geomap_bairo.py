# Generated by Django 4.2.11 on 2024-07-12 22:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0002_geomap_lat_geomap_lon_geomap_name_geomap_osm_id_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='geomap',
            name='bairo',
            field=models.CharField(default='N/A', max_length=200),
        ),
    ]
