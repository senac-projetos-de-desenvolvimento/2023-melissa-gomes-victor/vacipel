import requests
import matplotlib.pyplot as ptl
import base64
from io import BytesIO

def get_geo(address):
    url = "https://nominatim.openstreetmap.org/search"
    params = {
        "q": address,
        "format": "json",
        "limit": 1
    }
    headers = {
        "User-Agent": "Vacipel_TCC/1.0", 
        "Referer": "https://gitlab.com/senac-projetos-de-desenvolvimento/2023-melissa-gomes-victor/vacipel"  
    }
    response = requests.get(url, params=params, headers=headers)
    data = response.json()

    if data and len(data) > 0:
        lat = data[0]["lat"]
        lon = data[0]["lon"]
        place_id = data[0]["place_id"]
        osm_id = data[0]["osm_id"]
        name = data[0]["name"]
        return lat, lon, place_id, osm_id, name
    else:
        print("Sem Resultados")
        return None, None
    
    
def grafico():
    buffer = BytesIO()
    ptl.savefig(buffer, format='png')
    buffer.seek(0) 
    graph = base64.b64encode(buffer.read()).decode('utf-8')
    buffer.close()    
    return graph

def plot_grafico(x,y,tt):
    #ptl.switch_backend('AGG')
    ptl.figure(figsize=(10,7))
    ptl.title(tt)
    ptl.bar(x,y)
    ptl.xticks(rotation=45)
    ptl.xlabel('')
    ptl.ylabel('Total qtd')
    graph = grafico()
    return graph
    