from django.contrib import admin

from .models import GeoMap

admin.site.register(GeoMap)