from django.contrib.auth.models import User
from django.db import models

class GeoMap(models.Model):
    place_id = models.IntegerField(default=0)
    osm_id = models.IntegerField(default=0)
    lat = models.FloatField(default=0.0)
    lon = models.FloatField(default=0.0)
    name = models.CharField(max_length=200, default="N/A")
    bairo = models.CharField(max_length=200, default="N/A")
    user_pk = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}"
