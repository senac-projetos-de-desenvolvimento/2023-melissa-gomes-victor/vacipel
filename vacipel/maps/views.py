from django.shortcuts import render, redirect
from django.http import HttpResponse 
from django.views.generic.detail import DetailView
from .utils import get_geo, grafico,plot_grafico
from .models import GeoMap
from .forms import FilterForm
from django.db.models import *
from animals.models import *
from perfil.models import *
from django.db.models.functions import Coalesce
from django.views.generic import TemplateView
# from .utils import grafico
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


import folium
import os
from branca.element import Template, MacroElement

## Restringindo para somente administradores do sistema poderem acessar
@method_decorator(login_required, name='dispatch')
class Estatisticas(PermissionRequiredMixin,TemplateView):
    template_name = 'bases/estatisticas_admin.html'
    permission_required = 'auth.view_user'
    
    def get_context_data(self, **kwargs):
        animais_por_bairo =  Animal.objects.values('dono__perfil__endereco__bairo').annotate(item_count=Count('id'))
        context = super().get_context_data(**kwargs)        
        x_an = [x['dono__perfil__endereco__bairo'] for x in animais_por_bairo]
        y_an = [y['item_count'] for y in animais_por_bairo]
        chart_animal = plot_grafico(x_an,y_an,'Animais por Bairo')
        
        vacinas_por_animal_bairo =  Vacina.objects.values('animal_ref_vac_id__dono__perfil__endereco__bairo').annotate(item_count=Count('id'))
        x_vc = [x['animal_ref_vac_id__dono__perfil__endereco__bairo'] for x in vacinas_por_animal_bairo]
        y_vc = [y['item_count'] for y in vacinas_por_animal_bairo]
        chart_vacina = plot_grafico(x_vc,y_vc,'Vacinas por Bairo')
        
        usuarios_por_bairo =  Perfil.objects.values('user__perfil__endereco__bairo').annotate(item_count=Count('id'))
        x_ub = [x['user__perfil__endereco__bairo'] for x in usuarios_por_bairo]
        y_ub = [y['item_count'] for y in usuarios_por_bairo]
        chart_users = plot_grafico(x_ub,y_ub,'Usuario por Bairo')
        
        animais_por_especie =  Animal.objects.values('especie').annotate(item_count=Count('id'))
        x_ae = [x['especie'] for x in animais_por_especie]
        y_ae = [y['item_count'] for y in animais_por_especie]
        chart_especie = plot_grafico(x_ae,y_ae,'Qtd Animal Especie')
        
        # usuarios_em_total =  Animal.objects.values('especie').annotate(item_count=Count('id'))
        # x_ae = [x['especie'] for x in animais_por_especie]
        # y_ae = [y['item_count'] for y in animais_por_especie]
        # chart_especie = plot_grafico(x_ae,y_ae,'Qtd Animal Especie')
        #print(f'retorno: {animais_por_especie}')
        
        context['chart_animal'] =  chart_animal
        context['chart_vacina'] =  chart_vacina
        context['chart_users'] =  chart_users
        context['chart_especie'] =  chart_especie

        return context

class GeoMapView(TemplateView):
    template_name = 'bases/map.html'
    
    from branca.element import Template, MacroElement

    
    def get_context_data(self, **kwargs):
        animais_por_bairo =  Animal.objects.values('dono__perfil__endereco__bairo').annotate(item_count=Count('id'))
        vacinas_por_animal_bairo =  Vacina.objects.values('animal_ref_vac_id__dono__perfil__endereco__bairo').annotate(item_count=Count('id'))
        context = super().get_context_data(**kwargs)
        all_animals = Animal.objects.count()
        all_vacinas = Vacina.objects.count()
        all_users = User.objects.count()



        geo_path = os.path.join(os.path.dirname(__file__), '..', 'static', 'geo', 'pel.json')
        map_dict = {
            "Três Vendas":1,
            "Porto":2,
            "Laranjal":3,
            "Areal":4,
            "Centro":5,
            "Fragata":6,
            "Barragem":7
        }
        
        data = [[row['dono__perfil__endereco__bairo'], row['item_count']] for row in animais_por_bairo]
        response = [[map_dict[key], value] for key, value in data]
        
        
        def dados(dado):
            for i in dado:
                yield {"tittle":f"Qtd pets cadastrados no {i[0]}", "dado":i[1]}

        my_bairos = list(dados(data))

        items = [{"tittle":"Todos Animais", "dado":all_animals},
                {"tittle":"Quantas vacinas aplicadas", "dado":all_vacinas},
                ]
        for i in my_bairos:
            items.append(i)
        
        folium_map = folium.Map(location=[ -31.7281, -52.3227], zoom_start=12, tiles='OpenStreetMap')
        
        form = FilterForm(self.request.GET)

        if form.is_valid():
            filtro = form.cleaned_data['filtro']
            if filtro == 'Animais':
                data = [[row['dono__perfil__endereco__bairo'], row['item_count']] for row in animais_por_bairo]
                dados = [[map_dict[key], value] for key, value in data]
                legenda = "Quantidade de Animais Vacinados"
            else:
                data = [[row['animal_ref_vac_id__dono__perfil__endereco__bairo'], row['item_count']] for row in vacinas_por_animal_bairo]
                dados = [[map_dict[key], value] for key, value in data]
                legenda = "Quantidade Vacinas por Bairo"
        else:
            data = [[row['dono__perfil__endereco__bairo'], row['item_count']] for row in animais_por_bairo]
            dados = [[map_dict[key], value] for key, value in data]
            legenda = "Quantidade de Animais Vacinados"
            
        folium.Choropleth(
            geo_data=geo_path,
            name='choropleth',
            columns=['Bairos', 'Count'],
            data=dados,
            key_on='feature.id',
            fill_color='PiYG',
            fill_opacity=0.7,
            line_opacity=0.7,
            nan_fill_color='white',
            legend_name=legenda
        ).add_to(folium_map)

    
        style = lambda x: {'fillColor': '#ffffff', 
                            'color':'#000000', 
                            'fillOpacity': 0.1, 
                            'weight': 0.1}
        highlight = lambda x: {'fillColor': '#000000', 
                                        'color':'#000000', 
                                        'fillOpacity': 0.50, 
                                        'weight': 0.1}
        TIP = folium.features.GeoJson(
            geo_path,
            style_function=style, 
            control=False,
            highlight_function=highlight, 
            tooltip=folium.features.GeoJsonTooltip(
                fields=['bairo'],
                style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;") 
            )
        )
        # macro = MacroElement()
        

        
        folium_map.add_child(TIP)

        
        folium_map_html = folium_map._repr_html_()
        context['map'] = folium_map_html
        context['form'] = form 
        context['items'] = items

        return context