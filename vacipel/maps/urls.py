from django.urls import path
from . views import GeoMapView, Estatisticas

urlpatterns = [
    path('geo-map/', GeoMapView.as_view(), name='geo-map'),
    path('estatisticas/', Estatisticas.as_view(), name='estatisticas'),
]
