from django import forms

class FilterForm(forms.Form):
    filtro = forms.CharField(widget=forms.Select(choices=[('Animais', 'Animais'), ('Vacinas', 'Vacinas')], attrs={'class': 'form-select'}))
