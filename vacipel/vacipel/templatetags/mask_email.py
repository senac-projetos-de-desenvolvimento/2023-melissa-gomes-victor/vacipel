from django import template

register = template.Library()

@register.filter(name='mask_email')
def mask_email(email):
    """
    Masks an email address by replacing characters after a certain position with '*'.
    """
    if '@' in email:
        username, domain = email.split('@')
        masked_username = username[:3] + '*' * (len(username) - 6) + username[-3:]
        return f'{masked_username}@{domain}'
    else:
        return email
