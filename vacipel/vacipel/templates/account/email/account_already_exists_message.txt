{% extends "account/email/base_message.txt" %}
{% load i18n %}

{% block content %}{% autoescape off %}{% blocktrans %}Olá você está recebendo este email devido uma tentantiva de cadastrar uma conta em VACIPEL com o email abaixo:

{{ email }}

Porém, está conta já existe cadastrada com este email. em caso de esquecimento de usuario e senha
utilize o link abaixo para recuperar sua conta:

Caso contrario ignore este email e marque como spam.
{{ password_reset_url }}{% endblocktrans %}{% endautoescape %}{% endblock content %}
