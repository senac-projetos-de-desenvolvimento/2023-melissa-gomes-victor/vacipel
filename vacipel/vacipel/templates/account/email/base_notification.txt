{% extends "account/email/base_message.txt" %}
{% load account %}
{% load i18n %}

{% block content %}{% autoescape off %}{% blocktrans %}Você está recebendo este email, devido uma alteração feita em sua conta:{% endblocktrans %}

{% block notification_message %}
{% endblock notification_message%}

{% blocktrans %}Se você não reconhece essa mudança, entre em contato com o time de desenvolvimento e segurança via email vacipelproject@gmail.com, A mudança é originada do seguinte local:

- IP: {{ip}}
- Navegador: {{user_agent}}
- Data: {{timestamp}}{% endblocktrans %}{% endautoescape %}{% endblock %}
